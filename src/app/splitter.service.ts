import {Injectable} from '@angular/core';
import {Gender, Preview, Token, TokenType} from "./types";
import {Constants} from "./constants";

@Injectable({
  providedIn: 'root'
})
export class SplitterService {

  titles = Constants.TITLES

  constructor() { }

  // starts splitting
  analyze(input: string) {

    if (input.match(/,.*,/))
      return "Sie können nur maximal ein Komma angeben"

    let address = "";
    let duplicateAddress = false;
    let titles: String[] = [];
    let names: Token[] = [];

    const tokenArray = this.tokenize(input)

    // splits up tokens in 3 categories
    tokenArray.forEach((token: Token) => {
      if (token.type == TokenType.Anrede) {
        if (address != "") duplicateAddress = true
        address = token.value
      } else if (token.type == TokenType.Titel) {
        titles.push(token.value)
      } else names.push(token)
    })

    if (duplicateAddress) return "Bitte geben Sie nur eine Anrede ein"

    // calculate name
    let commaOccurred = false;
    let firstNames = [];
    let lastNames: any[] = [];

    for (let i = 0; i < names.length; i++) {
      if (names[i].type == TokenType.Komma) commaOccurred = true
      else {
        if (i > 1 && names[i - 1].type == TokenType.Komma) {
          lastNames = firstNames;
          firstNames = []
        }
        firstNames.push(names[i])
      }
    }
    if (!commaOccurred) {
      // extract lastname
      while (firstNames.length > 1 && firstNames[firstNames.length - 1].type == TokenType.Adelstitel) {
        lastNames.push(firstNames.pop())
      }
      lastNames.push(firstNames.pop())
      while (firstNames.length > 1 && firstNames[firstNames.length - 1].type == TokenType.Adelstitel) {
        lastNames.push(firstNames.pop())
      }
      lastNames.reverse()
    }

    const preview: Preview = {
      address: address,
      title: titles.join(" "),
      firstname: firstNames.map(t => t.value).join(" "),
      lastname: lastNames.map(t => t.value).join(" "),
      gender: Constants.GENDER_MAP.get(address) || Gender.diverse
    }


    return preview
  }

  // turns words into tokens
  tokenize(input: string) {
    let lexArray: string[] = input.replace(","," , ").split(/\s+/g)
    const tokenArray: Token[] = lexArray.map((lex) => {
      return {
        type: (Constants.ADDRESSES.includes(lex)) ? TokenType.Anrede : (this.titles.includes(lex)) ? TokenType.Titel : (Constants.NOBLE_TITLES.includes(lex)) ? TokenType.Adelstitel : (lex == ",") ? TokenType.Komma : TokenType.Name,
        value: lex,
      }
    })
    return  tokenArray
  }

}
