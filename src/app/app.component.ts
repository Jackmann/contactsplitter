import {Component, OnInit} from '@angular/core';
import {Preview} from "./types";
import {SplitterService} from "./splitter.service";
import {MatDialog} from "@angular/material/dialog";
import {TitleDialogComponent} from "./title-dialog/title-dialog.component";
import {Constants} from "./constants";
import {EditEntryDialogComponent} from "./edit-entry-dialog/edit-entry-dialog.component";
import {GreetingFieldComponent} from "./greeting-field/greeting-field.component";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  constructor(
    private splitterService: SplitterService,
    public settingsDialog: MatDialog,
    public editDialog: MatDialog,
    public greetingDialog: MatDialog,
    public snackBar: MatSnackBar
  ) {}

  preview: Preview = {} as Preview;
  input = "";
  TableData: Preview[] = []
  error: string = "";

  // starts analyzing process
  analyze(){
    this.error = ""
    const analyzedValue: Preview | string = this.splitterService.analyze(this.input)
    if (typeof analyzedValue === 'string') {
      this.preview = {} as Preview;
      this.error = analyzedValue
    } else {
      this.preview = analyzedValue
    }
  }

  // calculates greeting
  getLetterPreview(pre: Preview){
    if (!pre.address) {
      return "Sehr geehrte Damen und Herren"
    }
    let p = Constants.LETTER_MAP.get(pre.address);
    if (pre.title) p = `${p} ${pre.title}`;
    if (pre.firstname) p = `${p} ${pre.firstname}`;
    return `${p} ${pre.lastname}`;
  }

  // adds analyzed Input to Table
  addToTable(){
    if (this.preview.lastname) {
      this.TableData.push(this.preview)
      this.input = "";
      this.preview = {} as Preview;
    }
  }

  // copies greeting to clipboard
  copyToClipboard(text: string){
    navigator.clipboard.writeText(text).then(
    )
    this.snackBar.open("Copied To Clipboard","", {duration: 2000, verticalPosition: "top"});
  }

  // deletes data from table
  delFromTable(item: Preview) {
    this.TableData.splice(this.TableData.indexOf(item),1)
  }

  // opens greeting in a dialog
  showGreeting(item: Preview){
    this.greetingDialog.open(GreetingFieldComponent, {
      width: '400px'
      ,data: this.getLetterPreview(item)})
  }

  // opens dialog with all available titles
  openTitleDialog(){
    this.settingsDialog.open(TitleDialogComponent);
  }

  // opens a dialog to edit table data
  openEditDialog(p: Preview): void {
    this.editDialog.open(EditEntryDialogComponent, {
      width: '400px',
      data: p,
    });
  }

  ngOnInit(): void {
  }

}
