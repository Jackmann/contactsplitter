export enum TokenType {
  Anrede, Titel, Adelstitel, Komma, Name
}

export type Token = {
  type: TokenType,
  value: string,
}

export type Preview = {
  address: string,
  title: string
  firstname: string,
  lastname: string,
  gender: Gender,
}

export enum Gender {
  male="männlich",
  female="weiblich",
  diverse="divers"
}
