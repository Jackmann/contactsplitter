import { Component, OnInit } from '@angular/core';
import {SplitterService} from "../splitter.service";

@Component({
  selector: 'app-title-dialog',
  templateUrl: './title-dialog.component.html',
  styleUrls: ['./title-dialog.component.scss']
})
export class TitleDialogComponent implements OnInit {

  input = "";

  constructor(private splitterService: SplitterService) { }

  ngOnInit(): void {
  }

  getTitles() {
    return this.splitterService.titles
  }

  remove(title: string) {
    this.splitterService.titles.splice(this.splitterService.titles.indexOf(title),1)
  }

  add(){
    if (this.input) {
      this.splitterService.titles.push(this.input)
      this.input = "";
    }
  }

}
