import {TestBed} from '@angular/core/testing';

import {SplitterService} from './splitter.service';
import {Gender, Preview} from "./types";

describe('SplitterService', () => {
  let service: SplitterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SplitterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should validate "Frau Sandra Berger" correctly', () => {
    const correctOutput: Preview = {
      address: "Frau",
      firstname: "Sandra",
      title: "",
      lastname: "Berger",
      gender: Gender.female
    }

    const output = service.analyze("Frau Sandra Berger");
    expect(output).toEqual(correctOutput)
  });

  it('should validate "Herr Dr. Sandro Gutmensch" correctly', () => {
    const correctOutput: Preview = {
      address: "Herr",
      firstname: "Sandro",
      title: "Dr.",
      lastname: "Gutmensch",
      gender: Gender.male
    }

    const output = service.analyze("Herr Dr. Sandro Gutmensch");
    expect(output).toEqual(correctOutput)
  });

  it('should validate "Professor Heinreich Freiherr vom Wald" correctly', () => {
    const correctOutput: Preview = {
      address: "",
      firstname: "Heinreich",
      title: "Professor",
      lastname: "Freiherr vom Wald",
      gender: Gender.diverse
    }

    const output = service.analyze("Professor Heinreich Freiherr vom Wald");
    expect(output).toEqual(correctOutput)
  });

  it('should validate "Mrs. Doreen Faber" correctly', () => {
    const correctOutput: Preview = {
      address: "Mrs.",
      firstname: "Doreen",
      title: "",
      lastname: "Faber",
      gender: Gender.female
    }

    const output = service.analyze("Mrs. Doreen Faber");
    expect(output).toEqual(correctOutput)
  });

  it('should validate "Mme. Charlotte Noir" correctly', () => {
    const correctOutput: Preview = {
      address: "Mme.",
      firstname: "Charlotte",
      title: "",
      lastname: "Noir",
      gender: Gender.female
    }

    const output = service.analyze("Mme. Charlotte Noir");
    expect(output).toEqual(correctOutput)
  });

  //muss noch nicht abgedeckt werden
  xit('should validate "Estobar y Gonzales" correctly', () => {
    const correctOutput: Preview = {
      address: "",
      firstname: "",
      title: "",
      lastname: "",
      gender: Gender.male
    }

    const output = service.analyze("Estobar y Gonzales");
    expect(output).toEqual(correctOutput)
  });

  it('should validate "Frau Prof. Dr. rer. nat. Maria von Leuthäuser-Schnarrenberger" correctly with custom Titles' , () => {
    const correctOutput: Preview = {
      address: "Frau",
      firstname: "Maria",
      title: "Prof. Dr. rer. nat.",
      lastname: "von Leuthäuser-Schnarrenberger",
      gender: Gender.female
    }
    service.titles.push("rer.");
    service.titles.push("nat.");

    const output = service.analyze("Frau Prof. Dr. rer. nat. Maria von Leuthäuser-Schnarrenberger");
    expect(output).toEqual(correctOutput)
  });

  it('should validate "Herr Dipl. Ing. Max von Müller" correctly' , () => {
    const correctOutput: Preview = {
      address: "Herr",
      firstname: "Max",
      title: "Dipl. Ing.",
      lastname: "von Müller",
      gender: Gender.male
    }

    const output = service.analyze("Herr Dipl. Ing. Max von Müller");
    expect(output).toEqual(correctOutput)
  });

  it('should validate "Dr. Russwurm, Winfried" correctly' , () => {
    const correctOutput: Preview = {
      address: "",
      firstname: "Winfried",
      title: "Dr.",
      lastname: "Russwurm",
      gender: Gender.diverse
    }

    const output = service.analyze("Dr. Russwurm, Winfried");
    expect(output).toEqual(correctOutput)
  });

  xit('should validate "Herr Dr.-Ing. Dr. rer. nat. Dr. h.c. mult. Paul Steffens" correctly with custom Titles' , () => {
    const correctOutput: Preview = {
      address: "Herr",
      firstname: "Paul",
      title: "Dr.-Ing. Dr. rer. nat. Dr. h.c. mult.",
      lastname: "Steffens",
      gender: Gender.male
    }

    service.titles.push("Dr.-Ing.");
    service.titles.push("rer.");
    service.titles.push("nat.");
    service.titles.push("h.c.");
    service.titles.push("mult.");

    const output = service.analyze("Herr Dr.-Ing. Dr. rer. nat. Dr. h.c. mult. Paul Steffens");
    expect(output).toEqual(correctOutput)
  });
});
