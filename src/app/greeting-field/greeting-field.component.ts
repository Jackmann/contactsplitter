import {Component, Inject, Input, OnInit, Optional} from '@angular/core';
import {Constants} from "../constants";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {Preview} from "../types";

@Component({
  selector: 'app-greeting-field',
  templateUrl: './greeting-field.component.html',
  styleUrls: ['./greeting-field.component.scss']
})
export class GreetingFieldComponent implements OnInit {

  constructor(
    @Optional() @Inject(MAT_DIALOG_DATA) public data: string
  ) { }

  @Input() preview: string = ""

  ngOnInit(): void {
  }
}
