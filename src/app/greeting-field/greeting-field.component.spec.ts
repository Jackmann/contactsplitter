import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GreetingFieldComponent } from './greeting-field.component';

describe('GreetingFieldComponent', () => {
  let component: GreetingFieldComponent;
  let fixture: ComponentFixture<GreetingFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GreetingFieldComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GreetingFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
