import {Gender} from "./types";

export abstract class Constants {
  static readonly ADDRESSES: string[] = ['Herr','Herrn', 'Frau','Mrs','Ms','Mr','Mrs.','Ms.','Mr.','Signora','Sig.','Mme','M','Mme.','M.','Señóra','Señór'];
  static readonly TITLES: string[] = ['Dr', 'Dr.','Doktor','Prof','Prof.','Professor','med','med.','Dipl.','Ing.'];
  static readonly NOBLE_TITLES: string[] = ['vom','von','van','de','der','Freiherr'];

  static readonly GENDER_MAP: Map<string,Gender> = new Map<string,Gender>([
    ["Herr",Gender.male],
    ["Herrn",Gender.male],
    ["Frau",Gender.female],
    ["Mrs",Gender.female],
    ["Ms",Gender.female],
    ["Mr",Gender.male],
    ["Mrs.",Gender.female],
    ["Ms.",Gender.female],
    ["Mr.",Gender.male],
    ["Signora",Gender.female],
    ["Sig.",Gender.male],
    ["Mme",Gender.female],
    ["Ma",Gender.male],
    ["Mme.",Gender.female],
    ["Ma.",Gender.male],
    ["Señóra",Gender.female],
    ["Señór",Gender.male],
  ])
  static readonly LETTER_MAP: Map<string,string> = new Map<string,string>([
    ["Herr","Sehr geehrter Herr"],
    ["Herrn","Sehr geehrter Herr"],
    ["Frau","Sehr geehrte Frau"],
    ["Mrs","Dear Mrs"],
    ["Ms","Dear Ms"],
    ["Mr","Dear Mr"],
    ["Mrs.","Dear Mrs"],
    ["Ms.","Dear Ms"],
    ["Mr.","Dear Mr"],
    ["Signora","Gentile Signora"],
    ["Sig.","Egregio Signor"],
    ["Mme","Madame"],
    ["M","Monsieur"],
    ["Mme.","Madame"],
    ["M.","Monsieur"],
    ["Señora","Estimada Señoora"],
    ["Señor","Esitmado Señor"],
  ])
}
