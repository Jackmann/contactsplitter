import {Component, Inject, OnInit} from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import {Preview} from "../types";

@Component({
  selector: 'app-edit-entry-dialog',
  templateUrl: './edit-entry-dialog.component.html',
  styleUrls: ['./edit-entry-dialog.component.scss']
})
export class EditEntryDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Preview
  ) { }

  ngOnInit(): void {
  }

}
