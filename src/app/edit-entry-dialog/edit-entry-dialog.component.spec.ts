import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditEntryDialogComponent } from './edit-entry-dialog.component';
import {MAT_DIALOG_DATA, MatDialogModule} from "@angular/material/dialog";

describe('EditEntryDialogComponent', () => {
  let component: EditEntryDialogComponent;
  let fixture: ComponentFixture<EditEntryDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditEntryDialogComponent ],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {} },
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditEntryDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
